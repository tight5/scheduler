\c scheduler scheduler_api

BEGIN;

ALTER TABLE crops ADD COLUMN grams_per_box DECIMAL;
ALTER TABLE crops ADD COLUMN cost_per_box DECIMAL;

COMMIT;
