\c scheduler scheduler_api

BEGIN;

CREATE OR REPLACE FUNCTION set_updated_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TABLE IF NOT EXISTS crops(
    id                   SERIAL PRIMARY KEY,
    name                 VARCHAR(255),

    -- unit grams
    density              INT,

    soak_hours           INT,
    blackout_days        INT NOT NULL,
    ambient_days_before  INT,
    lit_days             INT,
    ambient_days_after   INT,

    harvest_window_days  INT,
    -- unit grams / tray
    expected_yield       DECIMAL,

    created              TIMESTAMPTZ DEFAULT now(),
    updated              TIMESTAMPTZ DEFAULT now(),
    deleted              TIMESTAMPTZ
);
CREATE TRIGGER auto_updated BEFORE UPDATE ON crops FOR EACH ROW EXECUTE PROCEDURE set_updated_timestamp();

INSERT INTO crops
(
    name,
    density,
    soak_hours,
    blackout_days,
    ambient_days_before,
    lit_days,
    ambient_days_after,
    harvest_window_days,
    expected_yield
)
VALUES
(
    'Sunflower',
    250,
    10,
    4,
    NULL,
    3,
    NULL,
    NULL,
    500
);
INSERT INTO crops
(
    name,
    density,
    soak_hours,
    blackout_days,
    ambient_days_before,
    lit_days,
    ambient_days_after,
    harvest_window_days,
    expected_yield
)
VALUES
(
    'Broccoli',
    30,
    NULL,
    6,
    1,
    5,
    NULL,
    3,
    200
);
INSERT INTO crops
(
    name,
    density,
    soak_hours,
    blackout_days,
    ambient_days_before,
    lit_days,
    ambient_days_after,
    harvest_window_days,
    expected_yield
)
VALUES
(
    'Field Pea',
    350,
    24,
    6,
    NULL,
    3,
    NULL,
    NULL,
    400
);
INSERT INTO crops
(
    name,
    density,
    soak_hours,
    blackout_days,
    ambient_days_before,
    lit_days,
    ambient_days_after,
    harvest_window_days,
    expected_yield
)
VALUES
(
    'Radish (Daikon)',
    30,
    24,
    4,
    NULL,
    2,
    NULL,
    NULL,
    150
);
INSERT INTO crops
(
    name,
    density,
    soak_hours,
    blackout_days,
    ambient_days_before,
    lit_days,
    ambient_days_after,
    harvest_window_days,
    expected_yield
)
VALUES
(
    'Kale ''Red Russian''',
    25,
    NULL,
    5,
    NULL,
    6,
    NULL,
    3,
    200
);
INSERT INTO crops
(
    name,
    density,
    soak_hours,
    blackout_days,
    ambient_days_before,
    lit_days,
    ambient_days_after,
    harvest_window_days,
    expected_yield
)
VALUES
(
    'Popcorn',
    100,
    12,
    10,
    NULL,
    NULL,
    NULL,
    2,
    100
);

CREATE TYPE Enviroment AS ENUM ('dev', 'prod');
CREATE TABLE IF NOT EXISTS racks(
    id                  SERIAL PRIMARY KEY,

    env                 Enviroment,

    lit                 BOOLEAN NOT NULL,
    tray_capacity       INT NOT NULL,

    notes               VARCHAR(625) DEFAULT '' NOT NULL,

    created             TIMESTAMPTZ DEFAULT now(),
    updated             TIMESTAMPTZ DEFAULT now(),
    deleted             TIMESTAMPTZ
);
CREATE TRIGGER auto_updated BEFORE UPDATE ON racks FOR EACH ROW EXECUTE PROCEDURE set_updated_timestamp();

INSERT INTO racks (env, lit, tray_capacity) VALUES ('prod', FALSE, 52);
INSERT INTO racks (env, lit, tray_capacity) VALUES ('prod', FALSE, 52);
INSERT INTO racks (env, lit, tray_capacity) VALUES ('prod', TRUE, 20);
INSERT INTO racks (env, lit, tray_capacity) VALUES ('prod', TRUE, 20);
INSERT INTO racks (env, lit, tray_capacity) VALUES ('prod', TRUE, 20);

CREATE TYPE Frequency AS ENUM ('once', 'daily', 'weekly', 'biweekly', 'monthly');

CREATE TABLE IF NOT EXISTS plans(
    id         SERIAL PRIMARY KEY,
    crop_id    INT NOT NULL REFERENCES crops(id),
    trays      INT NOT NULL,
    repeats    Frequency NOT NULL,

    start_date DATE NOT NULL,
    end_date   DATE DEFAULT NULL,

    notes      VARCHAR(625) DEFAULT '' NOT NULL,

    created    TIMESTAMPTZ DEFAULT now(),
    updated    TIMESTAMPTZ DEFAULT now(),
    deleted    TIMESTAMPTZ
);
CREATE TRIGGER auto_updated BEFORE UPDATE ON plans FOR EACH ROW EXECUTE PROCEDURE set_updated_timestamp();
CREATE INDEX IF NOT EXISTS plan_crop_id_idx ON plans (crop_id);


CREATE TABLE IF NOT EXISTS schedule(
    id                  SERIAL PRIMARY KEY,
    crop_id             INT NOT NULL REFERENCES crops(id),
    plan_id             INT NOT NULL REFERENCES plans(id),

    trays               INT NOT NULL,

    soak_date           TIMESTAMPTZ,
    plant_date          TIMESTAMPTZ NOT NULL,
    ambient_before_date DATE,
    lit_date            DATE,
    ambient_after_date  DATE,
    harvest_date        DATE NOT NULL,

    notes               VARCHAR(625) DEFAULT '' NOT NULL,

    created             TIMESTAMPTZ DEFAULT now(),
    updated             TIMESTAMPTZ DEFAULT now(),
    deleted             TIMESTAMPTZ
);
CREATE TRIGGER auto_updated BEFORE UPDATE ON schedule FOR EACH ROW EXECUTE PROCEDURE set_updated_timestamp();
CREATE INDEX IF NOT EXISTS schedule_plan_id_idx ON schedule (plan_id);
CREATE INDEX IF NOT EXISTS schedule_plant_date_idx ON schedule (plant_date);
CREATE INDEX IF NOT EXISTS schedule_harvest_date_idx ON schedule (harvest_date);

CREATE TABLE IF NOT EXISTS plantings(
    id                  SERIAL PRIMARY KEY,
    crop_id             INT NOT NULL REFERENCES crops(id),
    schedule_id         INT REFERENCES schedule(id),
    plan_id             INT REFERENCES plans(id),
    trays               INT NOT NULL,
    density             INT,

    soak_date           TIMESTAMPTZ,
    plant_date          TIMESTAMPTZ NOT NULL,
    ambient_before_date TIMESTAMPTZ,
    lit_date            TIMESTAMPTZ,
    ambient_after_date  TIMESTAMPTZ,
    harvest_date        TIMESTAMPTZ,

    -- units grams
    yield               DECIMAL,

    notes               VARCHAR(625) DEFAULT '' NOT NULL,

    created             TIMESTAMPTZ DEFAULT now(),
    updated             TIMESTAMPTZ DEFAULT now(),
    deleted             TIMESTAMPTZ
);
CREATE TRIGGER auto_updated BEFORE UPDATE ON plantings FOR EACH ROW EXECUTE PROCEDURE set_updated_timestamp();
CREATE INDEX IF NOT EXISTS plantings_crop_id_idx ON plantings (crop_id);
CREATE INDEX IF NOT EXISTS plantings_schedule_id_idx ON plantings (schedule_id);
CREATE INDEX IF NOT EXISTS plantings_plan_id_idx ON plantings (plan_id);
CREATE INDEX IF NOT EXISTS plantings_plant_date_idx ON plantings (plant_date);
CREATE INDEX IF NOT EXISTS plantings_harvest_date_idx ON plantings (harvest_date);
CREATE INDEX IF NOT EXISTS plantings_yield_idx ON plantings (yield);

COMMIT;
