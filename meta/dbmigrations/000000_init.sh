#!/bin/bash
# NOTE: the `-` prefix in the file ensure sorting will place it before 0 during init
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER scheduler_api PASSWORD 'tight5';
    CREATE DATABASE scheduler;
    GRANT ALL PRIVILEGES ON DATABASE scheduler TO scheduler_api;
EOSQL
