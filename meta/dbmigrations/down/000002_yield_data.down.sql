\c scheduler scheduler_api

BEGIN;

ALTER TABLE crops DROP COLUMN grams_per_box;
ALTER TABLE crops DROP COLUMN cost_per_box;

COMMIT;
