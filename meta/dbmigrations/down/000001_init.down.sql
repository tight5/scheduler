\c scheduler scheduler_api

BEGIN;

DROP FUNCTION IF EXISTS set_updated_timestamp()
DROP FUNCTION IF EXISTS set_deleted_timestamp()

DROP TABLE IF EXISTS crops;
DROP TABLE IF EXISTS racks;
DROP TABLE IF EXISTS plans;
DROP TABLE IF EXISTS schedule;
DROP TABLE IF EXISTS planting;

DROP TYPE IF EXISTS Enviroment;

COMMIT;
