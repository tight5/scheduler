RUN mkdir /go-mod/src/app
WORKDIR /go-mod/src/app

COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["app"]
