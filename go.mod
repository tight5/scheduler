module gitlab.com/tight5/scheduler

go 1.12

require (
	github.com/davecgh/go-spew v1.1.0
	github.com/go-kit/kit v0.9.0
	github.com/gogo/protobuf v1.3.1
	github.com/gorilla/mux v1.7.3
	github.com/lib/pq v1.2.0
	github.com/metaverse/truss v0.1.0
	github.com/pkg/errors v0.8.1
	gitlab.com/tight5/kit v0.0.0-20191118042331-980bef62c234
	google.golang.org/grpc v1.23.0
)

replace github.com/gogo/protobuf => github.com/metaverse/protobuf v1.3.2
