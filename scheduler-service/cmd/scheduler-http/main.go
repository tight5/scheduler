package main

import (
	"flag"
	"log"
	"net/http"
	"net/http/pprof"

	// This Service
	httptransport "github.com/go-kit/kit/transport/http"
	"gitlab.com/tight5/scheduler/scheduler-service/handlers"
	"gitlab.com/tight5/scheduler/scheduler-service/svc"
	"gitlab.com/tight5/scheduler/scheduler-service/svc/server"
)

func main() {
	flag.Parse()
	cfg := server.DefaultConfig

	endpoints := server.NewEndpoints()

	// Mechanical domain.
	errc := make(chan error)

	// Interrupt handler.
	go handlers.InterruptHandler(errc)

	// Debug listener.
	go func() {
		log.Println("transport", "debug", "addr", cfg.DebugAddr)

		m := http.NewServeMux()
		m.Handle("/debug/pprof/", http.HandlerFunc(pprof.Index))
		m.Handle("/debug/pprof/cmdline", http.HandlerFunc(pprof.Cmdline))
		m.Handle("/debug/pprof/profile", http.HandlerFunc(pprof.Profile))
		m.Handle("/debug/pprof/symbol", http.HandlerFunc(pprof.Symbol))
		m.Handle("/debug/pprof/trace", http.HandlerFunc(pprof.Trace))

		errc <- http.ListenAndServe(cfg.DebugAddr, m)
	}()

	// HTTP transport.
	go func() {
		log.Println("transport", "HTTP", "addr", cfg.HTTPAddr)
		h := svc.MakeHTTPHandler(endpoints,
			httptransport.ServerAfter(httptransport.SetResponseHeader("Access-Control-Allow-Origin", "*")),
			httptransport.ServerAfter(httptransport.SetResponseHeader("Access-Control-Allow-Methods", "*")),
			httptransport.ServerAfter(httptransport.SetResponseHeader("Access-Control-Allow-Headers", "*")),
		)
		errc <- http.ListenAndServe(cfg.HTTPAddr, h)
	}()

	// Run!
	log.Println("exit", <-errc)
}
