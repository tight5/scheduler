package handlers

import (
	"gitlab.com/tight5/kit/middleware"
	pb "gitlab.com/tight5/scheduler"
	"gitlab.com/tight5/scheduler/scheduler-service/svc"
)

// WrapEndpoints accepts the service's entire collection of endpoints, so that a
// set of middlewares can be wrapped around every middleware (e.g., access
// logging and instrumentation), and others wrapped selectively around some
// endpoints and not others (e.g., endpoints requiring authenticated access).
// Note that the final middleware wrapped will be the outermost middleware
// (i.e. applied first)
func WrapEndpoints(in svc.Endpoints) svc.Endpoints {
	middleware.StandardMiddlewares(&in)
	return in
}

func WrapService(in pb.SchedulerServer) pb.SchedulerServer {
	return in
}
