package handlers

import (
	"context"
	"fmt"
	"log"
	"math"
	"os"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"

	"gitlab.com/tight5/kit/db"
	"gitlab.com/tight5/kit/truss"
	pb "gitlab.com/tight5/scheduler"
	"gitlab.com/tight5/scheduler/scheduler-service/internal/postgres"
)

// NewService returns a naïve, stateless implementation of Service.
func NewService() pb.SchedulerServer {
	env := os.Getenv("env")
	truss.Init(env, "scheduler")

	dbClient, err := postgres.NewPostgres(db.Config{
		Host:   os.Getenv("POSTGRES_HOST"),
		Port:   5432,
		User:   os.Getenv("POSTGRES_USER"),
		Pass:   os.Getenv("POSTGRES_PASS"),
		DBName: "scheduler",
	})
	if err != nil {
		log.Fatal(err)
	}

	return schedulerService{
		DB: dbClient,
	}
}

type schedulerService struct {
	DB postgres.DBClient
}

// CropsGet implements Service.
func (s schedulerService) CropsGet(ctx context.Context, in *pb.Empty) (*pb.Crops, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()
	resp, err := s.DB.GetCrops(ctx)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// CropCreate implements Service.
func (s schedulerService) CropCreate(ctx context.Context, in *pb.Crop) (*pb.Crop, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	id, err := s.DB.CreateCrop(ctx, in)
	if err != nil {
		return nil, err
	}

	in.Id = id

	return in, nil
}

// CropDeleteByID implements Service.
func (s schedulerService) CropDeleteByID(ctx context.Context, in *pb.DeleteByIDRequest) (*pb.Empty, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	if in.GetId() == 0 {
		return nil, errors.New("must provide id")
	}

	err := s.DB.DeleteCropByID(ctx, in.GetId())
	if err != nil {
		return nil, err
	}

	return &pb.Empty{}, nil
}

// CropUpdate implements Service.

// RacksGet implements Service.
func (s schedulerService) RacksGet(ctx context.Context, in *pb.Empty) (*pb.Racks, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()
	resp, err := s.DB.GetRacks(ctx)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// RackCreate implements Service.
func (s schedulerService) RackCreate(ctx context.Context, in *pb.Rack) (*pb.Rack, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	id, err := s.DB.CreateRack(ctx, in)
	if err != nil {
		return nil, err
	}

	in.Id = id

	return in, nil
}

// RackDeleteByID implements Service.
func (s schedulerService) RackDeleteByID(ctx context.Context, in *pb.DeleteByIDRequest) (*pb.Empty, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	if in.GetId() == 0 {
		return nil, errors.New("must provide id")
	}

	err := s.DB.DeleteRackByID(ctx, in.GetId())
	if err != nil {
		return nil, err
	}

	return &pb.Empty{}, nil
}

// RackAddNote implements Service.
func (s schedulerService) RackAddNote(ctx context.Context, in *pb.Note) (*pb.Rack, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	in.Note = fmt.Sprintf("\n\n%s\n%s", time.Now().Format("2006-01-02 15:04:05 MST"), in.GetNote())
	notes, err := s.DB.AddNote(ctx, postgres.EntityRack, in)
	if err != nil {
		return nil, err
	}
	return &pb.Rack{
		Id:    in.GetId(),
		Notes: strings.TrimSpace(notes),
	}, nil
}

// ScheduleGet implements Service.
func (s schedulerService) ScheduleGet(ctx context.Context, in *pb.GetScheduleRequest) (*pb.Schedule, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()
	plans, err := s.DB.GetPlans(ctx)
	if err != nil {
		return nil, err
	}

	var (
		schd          = &pb.Schedule{}
		scheduleItems = make([]*pb.ScheduleItem, 0)
	)

	for _, plan := range plans {
		if in.CropId > 0 && in.CropId != plan.CropId {
			continue
		}
		crop, err := s.DB.GetCropByID(ctx, plan.CropId)
		if err != nil {
			return nil, errors.Wrap(err, "failed getting crop")
		}

		var soakDays int64 = 0
		//soakDays := int64(math.Floor(crop.SoakHours / 24.0))

		cur := *plan.DateRangeStart
		cur = time.Date(cur.Year(), cur.Month(), cur.Day(), 0, 0, 0, 0, time.Local)
		cur = cur.AddDate(0, 0, -int(crop.DaysOnSystem+soakDays))
		end := time.Now().AddDate(0, 0, 28*3)
		if plan.DateRangeEnd != nil {
			end = *plan.DateRangeEnd
		}
		end = time.Date(end.Year(), end.Month(), end.Day(), 0, 0, 0, 0, time.Local)
		end = end.AddDate(0, 0, -int(crop.DaysOnSystem+soakDays))

		var repeatsInDays int
		switch plan.Repeats {
		case pb.Frequency_once:
			// handled below in schedule building loop
		case pb.Frequency_daily:
			repeatsInDays = 1
		case pb.Frequency_weekly:
			repeatsInDays = 7
		case pb.Frequency_biweekly:
			repeatsInDays = 14
		case pb.Frequency_monthly:
			repeatsInDays = 28
		default:
			return nil, errors.New("invalid repeat frequency")
		}

		for !cur.After(end) {
			date := cur
			var item = pb.ScheduleItem{
				CropId: plan.CropId,
				Crop:   crop,
				PlanId: plan.Id,
				Trays:  plan.Trays,
			}

			item = populateScheduleItem(item, *crop, date)
			scheduleItems = append(scheduleItems, &item)
			cur = cur.AddDate(0, 0, repeatsInDays)

			if plan.Repeats == pb.Frequency_once {
				break
			}
		}
	}

	schd.Crop = cropSchedule(scheduleItems)
	caps := capacity(scheduleItems)
	schd.Capacity = caps
	return schd, nil
}

func stripTime(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func cropSchedule(items []*pb.ScheduleItem) []*pb.ScheduleDay {
	var (
		oldestPlant, farthestHarvest = scheduleBounds(items)

		soakDays          = map[string][]*pb.ScheduleItem{}
		plantDays         = map[string][]*pb.ScheduleItem{}
		ambientBeforeDays = map[string][]*pb.ScheduleItem{}
		litDays           = map[string][]*pb.ScheduleItem{}
		ambientAfterDays  = map[string][]*pb.ScheduleItem{}
		harvestDays       = map[string][]*pb.ScheduleItem{}

		days = []*pb.ScheduleDay{}
	)

	for _, item := range items {
		var (
			plantKey   = stripTime(*item.PlantDate).String()
			harvestKey = stripTime(*item.HarvestDate).String()
		)

		if item.SoakDate != nil {
			soakKey := stripTime(*item.SoakDate).String()
			soakDays[soakKey] = append(soakDays[soakKey], item)
		}
		plantDays[plantKey] = append(plantDays[plantKey], item)
		if item.AmbientBeforeDate != nil {
			ambientKey := stripTime(*item.AmbientBeforeDate).String()
			ambientBeforeDays[ambientKey] = append(ambientBeforeDays[ambientKey], item)
		}
		if item.LitDate != nil {
			litKey := stripTime(*item.LitDate).String()
			litDays[litKey] = append(litDays[litKey], item)
		}
		if item.AmbientAfterDate != nil {
			ambientKey := stripTime(*item.AmbientAfterDate).String()
			ambientAfterDays[ambientKey] = append(ambientAfterDays[ambientKey], item)
		}
		harvestDays[harvestKey] = append(harvestDays[harvestKey], item)
	}

	for i := 0; oldestPlant.Add(time.Hour * 24 * time.Duration(i)).Before(farthestHarvest); i++ {
		then := oldestPlant.Add(time.Hour * 24 * time.Duration(i))
		then = stripTime(then)
		var (
			dayOfWeek    = then.Weekday()
			morningStart time.Duration
		)

		switch dayOfWeek {
		// Early Days
		case time.Wednesday, time.Saturday, time.Sunday:
			morningStart = time.Hour * 7
		default:
			morningStart = time.Hour * 10
		}
		morn := then.Add(morningStart)
		day := pb.ScheduleDay{
			Soak:          soakDays[then.String()],
			Plant:         plantDays[then.String()],
			AmbientBefore: ambientBeforeDays[then.String()],
			Lit:           litDays[then.String()],
			AmbientAfter:  ambientAfterDays[then.String()],
			Harvest:       harvestDays[then.String()],
			Date:          &morn,
		}
		days = append(days, &day)
	}
	return days
}

// ScheduleItemAddNote implements Service.
func (s schedulerService) ScheduleItemAddNote(ctx context.Context, in *pb.Note) (*pb.ScheduleItem, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	in.Note = fmt.Sprintf("\n\n%s\n%s", time.Now().Format("2006-01-02 15:04:05 MST"), in.GetNote())
	notes, err := s.DB.AddNote(ctx, postgres.EntityScheduleItem, in)
	if err != nil {
		return nil, err
	}
	return &pb.ScheduleItem{
		Id:    in.GetId(),
		Notes: notes,
	}, nil
}

// PlantingsGet implements Service.
func (s schedulerService) PlantingsGet(ctx context.Context, in *pb.GetPlantingsRequest) (*pb.Plantings, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()
	resp, err := s.DB.GetPlantings(ctx)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

// PlantingCreate implements Service.
func (s schedulerService) PlantingCreate(ctx context.Context, in *pb.Planting) (*pb.Planting, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	id, err := s.DB.CreatePlanting(ctx, in)
	if err != nil {
		return nil, err
	}
	in.Id = id
	return in, nil
}

// PlantingAddNote implements Service.
func (s schedulerService) PlantingAddNote(ctx context.Context, in *pb.Note) (*pb.Planting, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	in.Note = fmt.Sprintf("\n\n%s\n%s", time.Now().Format("2006-01-02 15:04:05 MST"), in.GetNote())
	notes, err := s.DB.AddNote(ctx, postgres.EntityPlanting, in)
	if err != nil {
		return nil, err
	}
	return &pb.Planting{
		Id:    in.GetId(),
		Notes: notes,
	}, nil
}

// Status implements Service.
func (s schedulerService) Status(ctx context.Context, in *pb.StatusRequest) (*pb.StatusResponse, error) {
	var resp pb.StatusResponse
	resp = pb.StatusResponse{
		Status: pb.ServiceStatus_OK,
	}
	return &resp, nil
}

// SchedulePlan implements Service.
func (s schedulerService) SchedulePlan(ctx context.Context, in *pb.Plan) (*pb.Schedule, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	if in.DateRangeStart == nil {
		return nil, errors.New("must provide date_range_start")
	}

	if in.DateRangeEnd != nil && in.DateRangeEnd.Before(time.Now()) {
		return nil, errors.New("plan end date is in the past")
	}

	var dayOfWeek = in.DateRangeStart.Weekday()
	plan, err := s.DB.GetActivePlan(ctx, in.CropId, in.Repeats, dayOfWeek)
	if err != nil {
		return nil, errors.Wrap(err, "failed looking for active plan")
	}
	if plan != nil {
		return nil, errors.Errorf("plan for crop '%d' already exists on day of week '%s'", plan.CropId, dayOfWeek.String())
	}

	planID, err := s.DB.CreatePlan(ctx, in)
	if err != nil {
		return nil, err
	}
	in.Id = planID

	/*
		err = s.DB.CreateScheduleItems(ctx, schedule)
		if err != nil {
			return nil, errors.Wrap(err, "failed creating schedule")
		}
	*/

	return &pb.Schedule{
		Crop:  nil,
		Plans: []*pb.Plan{in},
	}, nil
}

func populateScheduleItem(item pb.ScheduleItem, crop pb.Crop, start time.Time) pb.ScheduleItem {
	var ptr = func(t time.Time) *time.Time {
		return &t
	}
	_ = ptr

	item.PlantDate = ptr(stripTime(start))
	if crop.SoakHours.GetValue() != 0 {
		sHs := crop.SoakHours.GetValue()
		var offset int
		if sHs > 12 {
			offset = 1
			offset += int(math.Floor(float64(sHs-12) / 24.0))
		}

		date := start.AddDate(0, 0, int(-offset))
		item.SoakDate = &date
		item.PlantDate = ptr(date.Add(time.Hour * time.Duration(crop.SoakHours.GetValue())))

	}

	lit := start.AddDate(0, 0, int(crop.BlackoutDays))
	if crop.LitDays.GetValue() != 0 {
		item.LitDate = &lit
	}
	if crop.AmbientDaysBefore.GetValue() != 0 {
		date := lit
		item.AmbientBeforeDate = &date
		item.LitDate = ptr(date.AddDate(0, 0, int(crop.AmbientDaysBefore.GetValue())))
	}

	harvest := lit
	if item.LitDate != nil {
		harvest = item.LitDate.AddDate(0, 0, int(crop.LitDays.GetValue()))
	}
	item.HarvestDate = &harvest
	if crop.AmbientDaysAfter.GetValue() != 0 {
		date := harvest
		item.AmbientAfterDate = &date
		item.HarvestDate = ptr(date.AddDate(0, 0, int(crop.AmbientDaysAfter.GetValue())))
	}

	return item
}

// Capacity implements Service.
func (s schedulerService) Capacity(ctx context.Context, in *pb.Empty) (*pb.CapacitySchedule, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()
	schd, err := s.DB.GetSchedule(ctx)
	if err != nil {
		return nil, err
	}

	caps := capacity(schd)

	return &pb.CapacitySchedule{
		Capacity: caps,
	}, nil
}

func scheduleBounds(schd []*pb.ScheduleItem) (time.Time, time.Time) {
	var (
		oldestPlant     = time.Now()
		farthestHarvest time.Time
	)
	for _, item := range schd {
		t := *item.HarvestDate
		t = time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
		if item.HarvestDate.After(farthestHarvest) {
			farthestHarvest = *item.HarvestDate
		}
		if item.PlantDate.Before(oldestPlant) {
			oldestPlant = *item.PlantDate
		}
	}
	return oldestPlant, farthestHarvest
}

func capacity(schd []*pb.ScheduleItem) []*pb.Capacity {
	var (
		oldestPlant, farthestHarvest = scheduleBounds(schd)
		capMap                       = map[string]pb.Capacity{}
	)

	for _, item := range schd {
		for _, t := range daysBetween(*item.PlantDate, earliestNotNil(item.AmbientBeforeDate, item.LitDate, item.AmbientAfterDate)) {
			t = stripTime(t)
			c := capMap[t.String()]
			c.Blackout += item.Trays
			capMap[t.String()] = c
		}
		for _, t := range daysBetween(earliestNotNil(item.AmbientBeforeDate, item.LitDate, item.AmbientAfterDate), *item.HarvestDate) {
			t = stripTime(t)
			c := capMap[t.String()]
			c.Lit += item.Trays
			capMap[t.String()] = c
		}
		/*
			// harvestable days
			for _, t := range daysBetween(*item.HarvestDate, item.HarvestDate.AddDate(0, 0, int(item.Crop.HarvestWindowDays.GetValue()))) {
				t = time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
				if t.After(farthestHarvest) {
					farthestHarvest = t
				}
				capMapHarvest[t.String()] += item.Trays
			}
		*/
		t := stripTime(*item.HarvestDate)
		c := capMap[t.String()]
		c.Harvest += item.Trays
		c.ExpectedYield += int64(item.Crop.ExpectedYield.GetValue()) * item.Trays
		expectedBoxes := int64(math.Floor((item.Crop.ExpectedYield.GetValue() * float64(item.Trays)) / item.Crop.GramsPerBox.GetValue()))
		c.ExpectedBoxes += expectedBoxes
		c.PotentialRevenue += float64(expectedBoxes) * item.Crop.CostPerBox.GetValue()
		capMap[t.String()] = c
	}
	var (
		caps = []*pb.Capacity{}
	)
	oldestPlant = time.Date(oldestPlant.Year(), oldestPlant.Month(), oldestPlant.Day(), 0, 0, 0, 0, oldestPlant.Location())
	for i := 0; oldestPlant.Add(time.Hour * 24 * time.Duration(i)).Before(farthestHarvest); i++ {
		then := oldestPlant.Add(time.Hour * 24 * time.Duration(i))
		then = time.Date(then.Year(), then.Month(), then.Day(), 0, 0, 0, 0, then.Location())
		c := capMap[then.String()]
		c.Date = &then
		caps = append(caps, &c)
	}
	return caps
}

func daysBetween(t1, t2 time.Time) []time.Time {
	if t1 == (time.Time{}) {
		return nil
	}
	if t1.After(t2) {
		return nil
	}
	ts := make([]time.Time, 0)
	for i := 0; ; i++ {
		t := t1.Add(time.Hour * 24 * time.Duration(i))
		if t.Before(t2) {
			ts = append(ts, t)
			continue
		}
		break

	}
	return ts
}

func latestNotNil(ts ...*time.Time) time.Time {
	for i := len(ts) - 1; i > 0; i-- {
		if ts[i] != nil {
			return *ts[i]
		}
	}
	return time.Time{}
}

func earliestNotNil(ts ...*time.Time) time.Time {
	for _, t := range ts {
		if t != nil {
			return *t
		}
	}
	return time.Time{}
}

// CropCreateOptions implements Service.
func (s schedulerService) CropCreateOptions(ctx context.Context, in *pb.Empty) (*pb.Empty, error) {
	var resp pb.Empty
	resp = pb.Empty{}
	return &resp, nil
}

// CropDeleteByIDOptions implements Service.

// CropUpdateByID implements Service.
func (s schedulerService) CropUpdateByID(ctx context.Context, in *pb.Crop) (*pb.Crop, error) {
	ctx, cnl := context.WithTimeout(ctx, time.Second*10)
	defer cnl()

	if in.GetId() == 0 {
		return nil, errors.New("must provide id")
	}

	err := s.DB.UpdateCrop(ctx, in)
	if err != nil {
		return nil, err
	}

	return in, nil
}

// CropByIDOptions implements Service.
func (s schedulerService) CropByIDOptions(ctx context.Context, in *pb.Empty) (*pb.Empty, error) {
	var resp pb.Empty
	resp = pb.Empty{}
	return &resp, nil
}

// UpdatePlan implements Service.

// PlanByIDOptions implements Service.
func (s schedulerService) PlanByIDOptions(ctx context.Context, in *pb.Empty) (*pb.Empty, error) {
	var resp pb.Empty
	resp = pb.Empty{}
	return &resp, nil
}

// PlanUpdateByID currently only supports updating trays
func (s schedulerService) PlanUpdateByID(ctx context.Context, in *pb.Plan) (*pb.Empty, error) {
	var ptr = func(t time.Time) *time.Time {
		return &t
	}

	plan, err := s.DB.GetPlanByID(ctx, in.Id)
	if err != nil {
		return nil, errors.Wrap(err, "failed fetching plan to update from db")
	}

	// End current plan
	plan.DateRangeEnd = ptr(in.DateRangeEnd.AddDate(0, 0, -1))
	err = s.DB.UpdatePlan(ctx, plan)
	if err != nil {
		return nil, errors.Wrap(err, "failed updating plan")
	}

	// don't create a new plan if trays was set to 0
	if in.Trays == 0 {
		return &pb.Empty{}, nil
	}

	plan.Trays = in.Trays
	plan.DateRangeStart = in.DateRangeEnd
	plan.DateRangeEnd = nil
	_, err = s.DB.CreatePlan(ctx, plan)
	if err != nil {
		return nil, err
	}

	return &pb.Empty{}, nil
}

// PlanOptions implements Service.
func (s schedulerService) PlanOptions(ctx context.Context, in *pb.Empty) (*pb.Empty, error) {
	var resp pb.Empty
	resp = pb.Empty{}
	return &resp, nil
}
