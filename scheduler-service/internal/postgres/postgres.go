package postgres

import (
	"context"
	"fmt"
	"time"

	"database/sql"

	"github.com/gogo/protobuf/types"
	"gitlab.com/tight5/kit/db"
	"gitlab.com/tight5/kit/logger"
	pb "gitlab.com/tight5/scheduler"
)

type EntityType int

const (
	EntityCrop EntityType = iota
	EntityRack
	EntityScheduleItem
	EntityPlanting
)

type DBClient interface {
	CreateCrop(context.Context, *pb.Crop) (int64, error)
	CreateRack(context.Context, *pb.Rack) (int64, error)
	CreatePlan(context.Context, *pb.Plan) (int64, error)
	CreateScheduleItems(context.Context, []*pb.ScheduleItem) error
	CreatePlanting(context.Context, *pb.Planting) (int64, error)

	AddNote(context.Context, EntityType, *pb.Note) (string, error)
	UpdateCrop(context.Context, *pb.Crop) error
	UpdatePlan(context.Context, *pb.Plan) error

	GetCrops(context.Context) (*pb.Crops, error)
	GetCropByID(context.Context, int64) (*pb.Crop, error)
	GetRacks(context.Context) (*pb.Racks, error)
	GetSchedule(context.Context) ([]*pb.ScheduleItem, error)
	GetPlantings(context.Context) (*pb.Plantings, error)
	GetActivePlan(context.Context, int64, pb.Frequency, time.Weekday) (*pb.Plan, error)
	GetPlanByID(context.Context, int64) (*pb.Plan, error)
	GetPlans(context.Context) ([]*pb.Plan, error)

	DeleteCropByID(context.Context, int64) error
	DeleteRackByID(context.Context, int64) error
	DeletePlanByID(context.Context, int64) error
	DeleteScheduleItemByID(context.Context, int64) error
	DeletePlantingByID(context.Context, int64) error
}

type postgresClient struct {
	client *db.DB
}

func NewPostgres(c db.Config) (DBClient, error) {
	c.Driver = "postgres"
	// disable ssl when running locally or in docker compose
	if c.Host == "localhost" || c.Host == "db" {
		c.SSLMode = "disable"
	}
	db, err := db.New(c)
	if err != nil {
		return nil, err
	}
	ctx, cnl := context.WithTimeout(context.Background(), time.Second*5)
	defer cnl()
	return &postgresClient{
		client: db,
	}, db.Ping(ctx)
}

func (pg *postgresClient) CreateCrop(ctx context.Context, crop *pb.Crop) (int64, error) {
	const q = "INSERT INTO crops (name, density, soak_hours, blackout_days, ambient_days_before, lit_days, ambient_days_after, harvest_window_days, expected_yield, grams_per_box, cost_per_box) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING id"
	var (
		id int64
	)
	err := pg.client.WithTx(ctx, func(tx *db.Tx) error {
		row := tx.QueryRow(ctx, q, crop.GetName(), crop.GetDensity(), crop.GetSoakHours().GetValue(), crop.GetBlackoutDays(), crop.GetAmbientDaysBefore().GetValue(), crop.GetLitDays().GetValue(), crop.GetAmbientDaysAfter().GetValue(), crop.GetHarvestWindowDays().GetValue(), crop.GetExpectedYield().GetValue(), crop.GetGramsPerBox().GetValue(), crop.GetCostPerBox().GetValue())
		return row.Scan(&id)
	})
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (pg *postgresClient) UpdateCrop(ctx context.Context, crop *pb.Crop) error {
	const q = "UPDATE crops SET name=$1, density=$2, soak_hours=$3, blackout_days=$4, ambient_days_before=$5, lit_days=$6, ambient_days_after=$7, harvest_window_days=$8, expected_yield=$9, grams_per_box=$10, cost_per_box=$11 WHERE id=$12 RETURNING id"
	var (
		id int64
	)
	err := pg.client.WithTx(ctx, func(tx *db.Tx) error {
		row := tx.QueryRow(ctx, q, crop.GetName(), crop.GetDensity(), crop.GetSoakHours().GetValue(), crop.GetBlackoutDays(), crop.GetAmbientDaysBefore().GetValue(), crop.GetLitDays().GetValue(), crop.GetAmbientDaysAfter().GetValue(), crop.GetHarvestWindowDays().GetValue(), crop.GetExpectedYield().GetValue(), crop.GetGramsPerBox().GetValue(), crop.GetCostPerBox().GetValue(), crop.GetId())
		return row.Scan(&id)
	})
	if err != nil {
		return err
	}
	return nil
}
func (pg *postgresClient) UpdatePlan(ctx context.Context, plan *pb.Plan) error {
	const q = "UPDATE plans SET crop_id=$1, trays=$2, repeats=$3, start_date=$4, end_date=$5 WHERE id=$6"
	err := pg.client.WithTx(ctx, func(tx *db.Tx) error {
		_, err := tx.Exec(ctx, q, plan.CropId, plan.Trays, plan.Repeats.String(), plan.GetDateRangeStart(), plan.GetDateRangeEnd(), plan.Id)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

func (pg *postgresClient) CreateRack(ctx context.Context, rack *pb.Rack) (int64, error) {
	const q = "INSERT INTO racks (env, lit, tray_capacity, notes) VALUES ($1, $2, $3, $4, $5) RETURNING id"
	var (
		id int64
	)
	err := pg.client.WithTx(ctx, func(tx *db.Tx) error {
		row := tx.QueryRow(ctx, q, rack.GetEnv().String(), rack.GetLit(), rack.GetTrayCapacity(), rack.GetNotes())
		return row.Scan(&id)
	})
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (pg *postgresClient) CreatePlan(ctx context.Context, plan *pb.Plan) (int64, error) {
	const q = "INSERT INTO plans (crop_id, trays, repeats, start_date, end_date, notes) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"
	var (
		id int64
	)
	err := pg.client.WithTx(ctx, func(tx *db.Tx) error {
		row := tx.QueryRow(ctx, q, plan.GetCropId(), plan.GetTrays(), plan.GetRepeats().String(), plan.GetDateRangeStart(), plan.GetDateRangeEnd(), plan.GetNotes())
		return row.Scan(&id)
	})
	if err != nil {
		return 0, err
	}
	return id, nil
}
func (pg *postgresClient) CreateScheduleItems(ctx context.Context, items []*pb.ScheduleItem) error {
	const q = "INSERT INTO schedule (crop_id, plan_id, trays, soak_date, plant_date, ambient_before_date, lit_date, ambient_after_date, harvest_date) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)"

	err := pg.client.WithTx(ctx, func(tx *db.Tx) error {
		for _, item := range items {
			_, err := tx.Exec(ctx,
				q,
				item.CropId,
				item.PlanId,
				item.Trays,
				item.SoakDate,
				item.PlantDate,
				item.AmbientBeforeDate,
				item.LitDate,
				item.AmbientAfterDate,
				item.HarvestDate,
			)
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

func (pg *postgresClient) CreatePlanting(ctx context.Context, planting *pb.Planting) (int64, error) {
	const q = "INSERT INTO plantings (crop_id, schedule_id, plan_id, trays, density, soak_date, plant_date, ambient_before_date, lit_date, ambient_after_date, harvest_date, yield) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING id"
	var (
		id int64
	)

	// Validation and conversion
	var (
		cropID     *int64
		scheduleID *int64
		planID     *int64
		yield      *float64
	)

	if p := planting.CropId; p != nil {
		cropID = &p.Value
	}
	if p := planting.ScheduleId; p != nil {
		scheduleID = &p.Value
	}
	if p := planting.PlanId; p != nil {
		planID = &p.Value
	}
	if y := planting.Yield; y != nil {
		yield = &y.Value
	}

	if cropID == nil {
		return 0, fmt.Errorf("crop_id cannot be null")
	}

	err := pg.client.WithTx(ctx, func(tx *db.Tx) error {
		row := tx.QueryRow(ctx,
			q,
			cropID,
			scheduleID,
			planID,
			planting.Trays,
			planting.Density,
			planting.SoakDate,
			planting.PlantDate,
			planting.AmbientBeforeDate,
			planting.LitDate,
			planting.AmbientAfterDate,
			planting.HarvestDate,
			yield,
		)
		return row.Scan(&id)
	})
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (pg *postgresClient) AddNote(ctx context.Context, entityType EntityType, n *pb.Note) (string, error) {
	const (
		crop     = "UPDATE crops "
		rack     = "UPDATE racks "
		schedule = "UPDATE schedule "
		planting = "UPDATE plantings "

		setQ = "SET notes = notes || $2 WHERE id = $1 RETURNING notes"
	)
	var notes string
	err := pg.client.WithTx(ctx, func(tx *db.Tx) error {
		var q string
		switch entityType {
		case EntityCrop:
			q = crop + setQ
		case EntityRack:
			q = rack + setQ
		case EntityScheduleItem:
			q = schedule + setQ
		case EntityPlanting:
			q = planting + setQ
		}
		logger.Info().Log("msg", q)
		row := tx.QueryRow(ctx, q, n.GetId(), n.GetNote())
		err := row.Scan(&notes)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return "", err
	}

	return notes, nil
}

func (pg *postgresClient) GetCrops(ctx context.Context) (*pb.Crops, error) {
	const (
		qDays = `
sum(COALESCE(blackout_days,0))
          + COALESCE(ambient_days_before,0)
          + COALESCE(lit_days,0)
          + COALESCE(ambient_days_after,0) AS days_on_system`

		q = "SELECT id, name, density, soak_hours, blackout_days, ambient_days_before, lit_days, ambient_days_after, harvest_window_days, expected_yield, grams_per_box, cost_per_box, %s FROM crops WHERE deleted is NULL GROUP BY id"
	)
	var crops []*pb.Crop
	rows, err := pg.client.Query(ctx, fmt.Sprintf(q, qDays))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			crop              pb.Crop
			soakHours         sql.NullInt64
			ambientDaysBefore sql.NullInt64
			litDays           sql.NullInt64
			ambientDaysAfter  sql.NullInt64
			harvestWindowDays sql.NullInt64

			expectedYield sql.NullFloat64
			gramsPerBox   sql.NullFloat64
			costPerBox    sql.NullFloat64
		)
		err := rows.Scan(&crop.Id, &crop.Name, &crop.Density, &soakHours, &crop.BlackoutDays, &ambientDaysBefore, &litDays, &ambientDaysAfter, &harvestWindowDays, &expectedYield, &gramsPerBox, &costPerBox, &crop.DaysOnSystem)
		if err != nil {
			return nil, err
		}
		if soakHours.Valid {
			crop.SoakHours = &types.Int64Value{Value: soakHours.Int64}
		}
		if ambientDaysBefore.Valid {
			crop.AmbientDaysBefore = &types.Int64Value{Value: ambientDaysBefore.Int64}
		}
		if litDays.Valid {
			crop.LitDays = &types.Int64Value{Value: litDays.Int64}
		}
		if ambientDaysAfter.Valid {
			crop.AmbientDaysAfter = &types.Int64Value{Value: ambientDaysAfter.Int64}
		}
		if harvestWindowDays.Valid {
			crop.HarvestWindowDays = &types.Int64Value{Value: harvestWindowDays.Int64}
		}
		if expectedYield.Valid {
			crop.ExpectedYield = &types.DoubleValue{Value: expectedYield.Float64}
		}
		if gramsPerBox.Valid {
			crop.GramsPerBox = &types.DoubleValue{Value: gramsPerBox.Float64}
		}
		if costPerBox.Valid {
			crop.CostPerBox = &types.DoubleValue{Value: costPerBox.Float64}
		}
		crops = append(crops, &crop)
	}
	if err != nil {
		return nil, err
	}

	return &pb.Crops{
		Crops: crops,
	}, nil
}

func (pg *postgresClient) GetCropByID(ctx context.Context, id int64) (*pb.Crop, error) {
	const (
		qDays = `
sum(COALESCE(blackout_days,0))
          + COALESCE(ambient_days_before,0)
          + COALESCE(lit_days,0)
          + COALESCE(ambient_days_after,0) AS days_on_system`

		q = "SELECT id, name, density, soak_hours, blackout_days, ambient_days_before, lit_days, ambient_days_after, harvest_window_days, expected_yield, grams_per_box, cost_per_box, %s FROM crops WHERE id = $1 GROUP BY id"
	)

	row := pg.client.QueryRow(ctx, fmt.Sprintf(q, qDays), id)
	var (
		crop              pb.Crop
		soakHours         sql.NullInt64
		ambientDaysBefore sql.NullInt64
		litDays           sql.NullInt64
		ambientDaysAfter  sql.NullInt64
		harvestWindowDays sql.NullInt64

		expectedYield sql.NullFloat64
		gramsPerBox   sql.NullFloat64
		costPerBox    sql.NullFloat64
	)
	err := row.Scan(&crop.Id, &crop.Name, &crop.Density, &soakHours, &crop.BlackoutDays, &ambientDaysBefore, &litDays, &ambientDaysAfter, &harvestWindowDays, &expectedYield, &gramsPerBox, &costPerBox, &crop.DaysOnSystem)
	if err != nil {
		return nil, err
	}
	if soakHours.Valid {
		crop.SoakHours = &types.Int64Value{Value: soakHours.Int64}
	}
	if ambientDaysBefore.Valid {
		crop.AmbientDaysBefore = &types.Int64Value{Value: ambientDaysBefore.Int64}
	}
	if litDays.Valid {
		crop.LitDays = &types.Int64Value{Value: litDays.Int64}
	}
	if ambientDaysAfter.Valid {
		crop.AmbientDaysAfter = &types.Int64Value{Value: ambientDaysAfter.Int64}
	}
	if harvestWindowDays.Valid {
		crop.HarvestWindowDays = &types.Int64Value{Value: harvestWindowDays.Int64}
	}
	if expectedYield.Valid {
		crop.ExpectedYield = &types.DoubleValue{Value: expectedYield.Float64}
	}
	if gramsPerBox.Valid {
		crop.GramsPerBox = &types.DoubleValue{Value: gramsPerBox.Float64}
	}
	if costPerBox.Valid {
		crop.CostPerBox = &types.DoubleValue{Value: costPerBox.Float64}
	}

	return &crop, nil
}

func (pg *postgresClient) GetRacks(ctx context.Context) (*pb.Racks, error) {
	const q = "SELECT id, env, lit, tray_capacity, notes FROM racks"
	var racks []*pb.Rack
	rows, err := pg.client.Query(ctx, q)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			rack pb.Rack
			lit  sql.NullBool
			env  string
		)
		rows.Scan(&rack.Id, &env, &lit, &rack.TrayCapacity, &rack.Notes)
		if lit.Valid {
			rack.Lit = &types.BoolValue{Value: lit.Bool}
		}
		rack.Env = pb.Rack_Environment(pb.Rack_Environment_value[env])
		racks = append(racks, &rack)
	}
	if err != nil {
		return nil, err
	}

	return &pb.Racks{
		Racks: racks,
	}, nil
}
func (pg *postgresClient) GetActivePlan(ctx context.Context, cropID int64, repeats pb.Frequency, dayOfWeek time.Weekday) (*pb.Plan, error) {
	const q = "SELECT id, crop_id, trays, start_date, end_date, notes FROM plans WHERE extract(dow from start_date) = $1 AND crop_id = $2 AND start_date < now() AND (end_date is NULL OR end_date > now()) AND repeats = $3 AND deleted is NULL"
	row := pg.client.QueryRow(ctx, q, int(dayOfWeek), cropID, repeats.String())
	var (
		plan pb.Plan
	)
	err := row.Scan(&plan.Id, &plan.CropId, &plan.Trays, &plan.DateRangeStart, &plan.DateRangeEnd, &plan.Notes)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	plan.Repeats = repeats

	return &plan, nil
}
func (pg *postgresClient) GetPlanByID(ctx context.Context, id int64) (*pb.Plan, error) {
	const q = "SELECT id, crop_id, trays, repeats, start_date, end_date, notes FROM plans WHERE id = $1"
	row := pg.client.QueryRow(ctx, q, id)
	var (
		plan    pb.Plan
		repeats string
	)
	err := row.Scan(&plan.Id, &plan.CropId, &plan.Trays, &repeats, &plan.DateRangeStart, &plan.DateRangeEnd, &plan.Notes)
	if err != nil {
		return nil, err
	}
	plan.Repeats = pb.Frequency(pb.Frequency_value[repeats])
	return &plan, nil
}
func (pg *postgresClient) GetPlans(ctx context.Context) ([]*pb.Plan, error) {
	const q = "SELECT id, crop_id, trays, repeats, start_date, end_date, notes FROM plans WHERE deleted is NULL"
	rows, err := pg.client.Query(ctx, q)
	if err != nil {
		return nil, err
	}

	var plans []*pb.Plan
	for rows.Next() {
		var (
			plan    pb.Plan
			repeats string
		)
		err := rows.Scan(&plan.Id, &plan.CropId, &plan.Trays, &repeats, &plan.DateRangeStart, &plan.DateRangeEnd, &plan.Notes)
		if err != nil {
			return nil, err
		}
		plan.Repeats = pb.Frequency(pb.Frequency_value[repeats])
		plans = append(plans, &plan)
	}
	if err != nil {
		return nil, err
	}

	return plans, nil
}
func (pg *postgresClient) GetSchedule(ctx context.Context) ([]*pb.ScheduleItem, error) {
	const q = "SELECT schedule.id, crop_id, plan_id, trays, soak_date, plant_date, ambient_before_date, lit_date, ambient_after_date, harvest_date, schedule.notes, crops.name, crops.density, crops.harvest_window_days FROM schedule JOIN crops ON schedule.crop_id = crops.id"
	var schedule []*pb.ScheduleItem
	rows, err := pg.client.Query(ctx, q)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var ptr = func(t time.Time) *time.Time {
		return &t
	}

	for rows.Next() {
		var (
			item              pb.ScheduleItem
			crop              pb.Crop
			harvestWindowDays sql.NullInt64
		)
		err := rows.Scan(&item.Id, &item.CropId, &item.PlanId, &item.Trays, &item.SoakDate, &item.PlantDate, &item.AmbientBeforeDate, &item.LitDate, &item.AmbientAfterDate, &item.HarvestDate, &item.Notes, &crop.Name, &crop.Density, &harvestWindowDays)
		if err != nil {
			return nil, err
		}
		item.Crop = &crop
		if item.SoakDate != nil {
			item.SoakDate = ptr(item.SoakDate.In(time.Local))
		}
		if item.PlantDate != nil {
			item.PlantDate = ptr(item.PlantDate.In(time.Local))
		}
		if item.AmbientBeforeDate != nil {
			item.AmbientBeforeDate = ptr(item.AmbientBeforeDate.In(time.Local))
		}
		if item.LitDate != nil {
			item.LitDate = ptr(item.LitDate.In(time.Local))
		}
		if item.AmbientAfterDate != nil {
			item.AmbientAfterDate = ptr(item.AmbientAfterDate.In(time.Local))
		}
		if item.HarvestDate != nil {
			item.HarvestDate = ptr(item.HarvestDate.In(time.Local))
		}
		if harvestWindowDays.Valid {
			item.Crop.HarvestWindowDays = &types.Int64Value{Value: harvestWindowDays.Int64}
		}
		schedule = append(schedule, &item)
	}
	if err != nil {
		return nil, err
	}

	return schedule, nil
}

func (pg *postgresClient) GetPlantings(ctx context.Context) (*pb.Plantings, error) {
	const q = "SELECT id, crop_id, schedule_id, plan_id, trays, density, soak_date, plant_date, ambient_before_date, lit_date, ambient_after_date, harvest_date, yield, notes FROM plantings"
	var plantings []*pb.Planting
	rows, err := pg.client.Query(ctx, q)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var planting pb.Planting
		var (
			cropID     sql.NullInt64
			scheduleID sql.NullInt64
			planID     sql.NullInt64
			yield      sql.NullFloat64
		)
		err := rows.Scan(
			&planting.Id,
			&cropID,
			&scheduleID,
			&planID,
			&planting.Trays,
			&planting.Density,
			&planting.SoakDate,
			&planting.PlantDate,
			&planting.AmbientBeforeDate,
			&planting.LitDate,
			&planting.AmbientAfterDate,
			&planting.HarvestDate,
			&yield,
			&planting.Notes,
		)
		if cropID.Valid {
			planting.CropId = &types.Int64Value{Value: cropID.Int64}
		}
		if scheduleID.Valid {
			planting.ScheduleId = &types.Int64Value{Value: scheduleID.Int64}
		}
		if planID.Valid {
			planting.PlanId = &types.Int64Value{Value: planID.Int64}
		}
		if yield.Valid {
			planting.Yield = &types.DoubleValue{Value: yield.Float64}
		}
		if err != nil {
			return nil, err
		}
		plantings = append(plantings, &planting)
	}

	return &pb.Plantings{
		Plantings: plantings,
	}, nil
}

func (pg *postgresClient) DeleteCropByID(ctx context.Context, id int64) error {
	const q = "UPDATE crops SET deleted = now() WHERE id = $1"
	return execByID(pg.client, ctx, q, id)
}
func (pg *postgresClient) DeleteRackByID(ctx context.Context, id int64) error {
	const q = "UPDATE racks SET deleted = now() WHERE id = $1"
	return execByID(pg.client, ctx, q, id)
}
func (pg *postgresClient) DeletePlanByID(ctx context.Context, id int64) error {
	const q = "UPDATE plans SET deleted = now() WHERE id = $1"
	return execByID(pg.client, ctx, q, id)
}
func (pg *postgresClient) DeleteScheduleItemByID(ctx context.Context, id int64) error {
	const q = "UPDATE schedule SET deleted = now() WHERE id = $1"
	return execByID(pg.client, ctx, q, id)
}
func (pg *postgresClient) DeletePlantingByID(ctx context.Context, id int64) error {
	const q = "DELETE FROM plantings WHERE id = $1"
	return execByID(pg.client, ctx, q, id)
}

func execByID(client *db.DB, ctx context.Context, q string, id int64) error {
	err := client.WithTx(ctx, func(tx *db.Tx) error {
		_, err := tx.Exec(ctx, q, id)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}
