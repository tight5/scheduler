// Code generated by truss. DO NOT EDIT.
// Rerunning truss will overwrite this file.
// Version: 8907ffca23
// Version Date: Wed 27 Nov 2019 09:28:21 PM UTC

package svc

// This file provides server-side bindings for the HTTP transport.
// It utilizes the transport/http.Server.

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"

	"context"

	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"

	// This service
	pb "gitlab.com/tight5/scheduler"
)

const contentType = "application/json; charset=utf-8"

var (
	_ = fmt.Sprint
	_ = bytes.Compare
	_ = strconv.Atoi
	_ = httptransport.NewServer
	_ = ioutil.NopCloser
	_ = pb.NewSchedulerClient
	_ = io.Copy
	_ = errors.Wrap
)

// MakeHTTPHandler returns a handler that makes a set of endpoints available
// on predefined paths.
func MakeHTTPHandler(endpoints Endpoints, options ...httptransport.ServerOption) http.Handler {
	serverOptions := []httptransport.ServerOption{
		httptransport.ServerBefore(headersToContext),
		httptransport.ServerErrorEncoder(errorEncoder),
		httptransport.ServerAfter(httptransport.SetContentType(contentType)),
	}
	serverOptions = append(serverOptions, options...)
	m := mux.NewRouter()

	m.Methods("GET").Path("/crops").Handler(httptransport.NewServer(
		endpoints.CropsGetEndpoint,
		DecodeHTTPCropsGetZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("POST").Path("/crop").Handler(httptransport.NewServer(
		endpoints.CropCreateEndpoint,
		DecodeHTTPCropCreateZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("OPTIONS").Path("/crop").Handler(httptransport.NewServer(
		endpoints.CropCreateOptionsEndpoint,
		DecodeHTTPCropCreateOptionsZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("DELETE").Path("/crop/{id}").Handler(httptransport.NewServer(
		endpoints.CropDeleteByIDEndpoint,
		DecodeHTTPCropDeleteByIDZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("OPTIONS").Path("/crop/{id}").Handler(httptransport.NewServer(
		endpoints.CropByIDOptionsEndpoint,
		DecodeHTTPCropByIDOptionsZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("PUT").Path("/crop/{id}").Handler(httptransport.NewServer(
		endpoints.CropUpdateByIDEndpoint,
		DecodeHTTPCropUpdateByIDZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("GET").Path("/racks").Handler(httptransport.NewServer(
		endpoints.RacksGetEndpoint,
		DecodeHTTPRacksGetZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("POST").Path("/rack").Handler(httptransport.NewServer(
		endpoints.RackCreateEndpoint,
		DecodeHTTPRackCreateZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("DELETE").Path("/rack/{id}").Handler(httptransport.NewServer(
		endpoints.RackDeleteByIDEndpoint,
		DecodeHTTPRackDeleteByIDZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("POST").Path("/rack/{id}/note").Handler(httptransport.NewServer(
		endpoints.RackAddNoteEndpoint,
		DecodeHTTPRackAddNoteZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("GET").Path("/schedule").Handler(httptransport.NewServer(
		endpoints.ScheduleGetEndpoint,
		DecodeHTTPScheduleGetZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("POST").Path("/schedule/{id}/note").Handler(httptransport.NewServer(
		endpoints.ScheduleItemAddNoteEndpoint,
		DecodeHTTPScheduleItemAddNoteZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("OPTIONS").Path("/plan").Handler(httptransport.NewServer(
		endpoints.PlanOptionsEndpoint,
		DecodeHTTPPlanOptionsZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("POST").Path("/plan").Handler(httptransport.NewServer(
		endpoints.SchedulePlanEndpoint,
		DecodeHTTPSchedulePlanZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("OPTIONS").Path("/plan/{id}").Handler(httptransport.NewServer(
		endpoints.PlanByIDOptionsEndpoint,
		DecodeHTTPPlanByIDOptionsZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("PUT").Path("/plan/{id}").Handler(httptransport.NewServer(
		endpoints.PlanUpdateByIDEndpoint,
		DecodeHTTPPlanUpdateByIDZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("GET").Path("/schedule/capacity").Handler(httptransport.NewServer(
		endpoints.CapacityEndpoint,
		DecodeHTTPCapacityZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("GET").Path("/plantings").Handler(httptransport.NewServer(
		endpoints.PlantingsGetEndpoint,
		DecodeHTTPPlantingsGetZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("POST").Path("/planting").Handler(httptransport.NewServer(
		endpoints.PlantingCreateEndpoint,
		DecodeHTTPPlantingCreateZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("POST").Path("/planting/{id}/note").Handler(httptransport.NewServer(
		endpoints.PlantingAddNoteEndpoint,
		DecodeHTTPPlantingAddNoteZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))

	m.Methods("GET").Path("/status").Handler(httptransport.NewServer(
		endpoints.StatusEndpoint,
		DecodeHTTPStatusZeroRequest,
		EncodeHTTPGenericResponse,
		serverOptions...,
	))
	return m
}

// ErrorEncoder writes the error to the ResponseWriter, by default a content
// type of application/json, a body of json with key "error" and the value
// error.Error(), and a status code of 500. If the error implements Headerer,
// the provided headers will be applied to the response. If the error
// implements json.Marshaler, and the marshaling succeeds, the JSON encoded
// form of the error will be used. If the error implements StatusCoder, the
// provided StatusCode will be used instead of 500.
func errorEncoder(_ context.Context, err error, w http.ResponseWriter) {
	body, _ := json.Marshal(errorWrapper{Error: err.Error()})
	if marshaler, ok := err.(json.Marshaler); ok {
		if jsonBody, marshalErr := marshaler.MarshalJSON(); marshalErr == nil {
			body = jsonBody
		}
	}
	w.Header().Set("Content-Type", contentType)
	if headerer, ok := err.(httptransport.Headerer); ok {
		for k := range headerer.Headers() {
			w.Header().Set(k, headerer.Headers().Get(k))
		}
	}
	code := http.StatusInternalServerError
	if sc, ok := err.(httptransport.StatusCoder); ok {
		code = sc.StatusCode()
	}
	w.WriteHeader(code)
	w.Write(body)
}

type errorWrapper struct {
	Error string `json:"error"`
}

// httpError satisfies the Headerer and StatusCoder interfaces in
// package github.com/go-kit/kit/transport/http.
type httpError struct {
	error
	statusCode int
	headers    map[string][]string
}

func (h httpError) StatusCode() int {
	return h.statusCode
}

func (h httpError) Headers() http.Header {
	return h.headers
}

// Server Decode

// DecodeHTTPCropsGetZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded cropsget request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPCropsGetZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Empty
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPCropCreateZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded cropcreate request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPCropCreateZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Crop
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPCropCreateOptionsZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded cropcreateoptions request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPCropCreateOptionsZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Empty
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPCropDeleteByIDZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded cropdeletebyid request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPCropDeleteByIDZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.DeleteByIDRequest
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	IdCropDeleteByIDStr := pathParams["id"]
	IdCropDeleteByID, err := strconv.ParseInt(IdCropDeleteByIDStr, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting IdCropDeleteByID from path, pathParams: %v", pathParams))
	}
	req.Id = IdCropDeleteByID

	return &req, err
}

// DecodeHTTPCropByIDOptionsZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded cropbyidoptions request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPCropByIDOptionsZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Empty
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPCropUpdateByIDZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded cropupdatebyid request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPCropUpdateByIDZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Crop
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	IdCropUpdateByIDStr := pathParams["id"]
	IdCropUpdateByID, err := strconv.ParseInt(IdCropUpdateByIDStr, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting IdCropUpdateByID from path, pathParams: %v", pathParams))
	}
	req.Id = IdCropUpdateByID

	return &req, err
}

// DecodeHTTPRacksGetZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded racksget request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPRacksGetZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Empty
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPRackCreateZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded rackcreate request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPRackCreateZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Rack
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPRackDeleteByIDZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded rackdeletebyid request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPRackDeleteByIDZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.DeleteByIDRequest
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	IdRackDeleteByIDStr := pathParams["id"]
	IdRackDeleteByID, err := strconv.ParseInt(IdRackDeleteByIDStr, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting IdRackDeleteByID from path, pathParams: %v", pathParams))
	}
	req.Id = IdRackDeleteByID

	return &req, err
}

// DecodeHTTPRackAddNoteZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded rackaddnote request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPRackAddNoteZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Note
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	IdRackAddNoteStr := pathParams["id"]
	IdRackAddNote, err := strconv.ParseInt(IdRackAddNoteStr, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting IdRackAddNote from path, pathParams: %v", pathParams))
	}
	req.Id = IdRackAddNote

	return &req, err
}

// DecodeHTTPScheduleGetZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded scheduleget request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPScheduleGetZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.GetScheduleRequest
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	if CropIdScheduleGetStrArr, ok := queryParams["crop_id"]; ok {
		CropIdScheduleGetStr := CropIdScheduleGetStrArr[0]
		CropIdScheduleGet, err := strconv.ParseInt(CropIdScheduleGetStr, 10, 64)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting CropIdScheduleGet from query, queryParams: %v", queryParams))
		}
		req.CropId = CropIdScheduleGet
	}

	return &req, err
}

// DecodeHTTPScheduleItemAddNoteZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded scheduleitemaddnote request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPScheduleItemAddNoteZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Note
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	IdScheduleItemAddNoteStr := pathParams["id"]
	IdScheduleItemAddNote, err := strconv.ParseInt(IdScheduleItemAddNoteStr, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting IdScheduleItemAddNote from path, pathParams: %v", pathParams))
	}
	req.Id = IdScheduleItemAddNote

	return &req, err
}

// DecodeHTTPPlanOptionsZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded planoptions request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPPlanOptionsZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Empty
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPSchedulePlanZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded scheduleplan request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPSchedulePlanZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Plan
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPPlanByIDOptionsZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded planbyidoptions request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPPlanByIDOptionsZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Empty
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPPlanUpdateByIDZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded planupdatebyid request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPPlanUpdateByIDZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Plan
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	IdPlanUpdateByIDStr := pathParams["id"]
	IdPlanUpdateByID, err := strconv.ParseInt(IdPlanUpdateByIDStr, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting IdPlanUpdateByID from path, pathParams: %v", pathParams))
	}
	req.Id = IdPlanUpdateByID

	return &req, err
}

// DecodeHTTPCapacityZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded capacity request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPCapacityZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Empty
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPPlantingsGetZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded plantingsget request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPPlantingsGetZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.GetPlantingsRequest
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	if CropIdPlantingsGetStrArr, ok := queryParams["crop_id"]; ok {
		CropIdPlantingsGetStr := CropIdPlantingsGetStrArr[0]
		CropIdPlantingsGet, err := strconv.ParseInt(CropIdPlantingsGetStr, 10, 64)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting CropIdPlantingsGet from query, queryParams: %v", queryParams))
		}
		req.CropId = CropIdPlantingsGet
	}

	return &req, err
}

// DecodeHTTPPlantingCreateZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded plantingcreate request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPPlantingCreateZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Planting
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	return &req, err
}

// DecodeHTTPPlantingAddNoteZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded plantingaddnote request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPPlantingAddNoteZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.Note
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	IdPlantingAddNoteStr := pathParams["id"]
	IdPlantingAddNote, err := strconv.ParseInt(IdPlantingAddNoteStr, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting IdPlantingAddNote from path, pathParams: %v", pathParams))
	}
	req.Id = IdPlantingAddNote

	return &req, err
}

// DecodeHTTPStatusZeroRequest is a transport/http.DecodeRequestFunc that
// decodes a JSON-encoded status request from the HTTP request
// body. Primarily useful in a server.
func DecodeHTTPStatusZeroRequest(_ context.Context, r *http.Request) (interface{}, error) {
	defer r.Body.Close()
	var req pb.StatusRequest
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot read body of http request")
	}
	if len(buf) > 0 {
		// AllowUnknownFields stops the unmarshaler from failing if the JSON contains unknown fields.
		unmarshaller := jsonpb.Unmarshaler{
			AllowUnknownFields: true,
		}
		if err = unmarshaller.Unmarshal(bytes.NewBuffer(buf), &req); err != nil {
			const size = 8196
			if len(buf) > size {
				buf = buf[:size]
			}
			return nil, httpError{errors.Wrapf(err, "request body '%s': cannot parse non-json request body", buf),
				http.StatusBadRequest,
				nil,
			}
		}
	}

	pathParams := mux.Vars(r)
	_ = pathParams

	queryParams := r.URL.Query()
	_ = queryParams

	if FullStatusStrArr, ok := queryParams["full"]; ok {
		FullStatusStr := FullStatusStrArr[0]
		FullStatus, err := strconv.ParseBool(FullStatusStr)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("Error while extracting FullStatus from query, queryParams: %v", queryParams))
		}
		req.Full = FullStatus
	}

	return &req, err
}

// EncodeHTTPGenericResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer. Primarily useful in a server.
func EncodeHTTPGenericResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	marshaller := jsonpb.Marshaler{
		EmitDefaults: false,
		OrigName:     true,
	}

	return marshaller.Marshal(w, response.(proto.Message))
}

// Helper functions

func headersToContext(ctx context.Context, r *http.Request) context.Context {
	for k, _ := range r.Header {
		// The key is added both in http format (k) which has had
		// http.CanonicalHeaderKey called on it in transport as well as the
		// strings.ToLower which is the grpc metadata format of the key so
		// that it can be accessed in either format
		ctx = context.WithValue(ctx, k, r.Header.Get(k))
		ctx = context.WithValue(ctx, strings.ToLower(k), r.Header.Get(k))
	}

	// Tune specific change.
	// also add the request url
	ctx = context.WithValue(ctx, "request-url", r.URL.Path)
	ctx = context.WithValue(ctx, "transport", "HTTPJSON")

	return ctx
}
